package com.q_rim.app2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class CrimeListFragment extends ListFragment {

  private static final String TAG = "---- CrimeListFragment";
  private ArrayList<Crime> crimes;
  private boolean subtitleVisible;

  @Override
  public void onListItemClick(ListView l, View v, int position, long id) {

    // ---- getting response from ListView touch:  1. get the object. ---- //
  //Crime c = (Crime)(getListAdapter()).getItem(position);            // Using ArrayAdapter
    Crime c = ((CrimeAdapter)getListAdapter()).getItem(position);     // Using CrimeAdapter

    Log.d(TAG, c.getTitle() + " was clicked");   Toast.makeText(getActivity().getApplicationContext(), c.getTitle() + " was clicked", Toast.LENGTH_SHORT).show();

    //Intent i = new Intent(getActivity(), CrimeActivity.class);    // Start CrimeActivity
    Intent i = new Intent(getActivity(), CrimePagerActivity.class);  // Start CrimePagerActivity <11.5>
    i.putExtra(CrimeFragment.EXTRA_CRIME_ID, c.getId());
    startActivity(i);
  }

  // Updating the ListView with changes from DetailedView
  @Override
  public void onResume() {
    super.onResume();
    ((CrimeAdapter)getListAdapter()).notifyDataSetChanged();
  }

  // Inflating an options menu <16.3>
  @Override
  public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    super.onCreateOptionsMenu(menu, inflater);
    inflater.inflate(R.menu.fragment_crime_list, menu);
    // Set menu item title based on subTitleVisible is true <16.20>
    MenuItem showSubtitle = menu.findItem(R.id.menu_item_show_subtitle);
    if (this.subtitleVisible && showSubtitle != null) {
      showSubtitle.setTitle((R.string.hide_subtitle));
    }
  }

  // Responding to ActionBar Menu selection <16.7>

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.menu_item_new_crime:
        Crime crime = new Crime();
        CrimeLabSingleton.get(getActivity()).addCrime(crime);
        Intent i = new Intent(getActivity(), CrimePagerActivity.class);
        i.putExtra(CrimeFragment.EXTRA_CRIME_ID, crime.getId());
        startActivityForResult(i, 0);
        return true;
      // case for Android API 11 which shows subtitle.
      case R.id.menu_item_show_subtitle:
        // Set subtitle if not shown.  Remove subtitle if shown.  <16.16>
        if (getActivity().getActionBar().getSubtitle() == null) {
          getActivity().getActionBar().setSubtitle(R.string.subtitle);
          this.subtitleVisible = true;
          item.setTitle(R.string.hide_subtitle);
        } else {
          getActivity().getActionBar().setSubtitle(null);
          this.subtitleVisible = false;
          item.setTitle(R.string.show_subtitle);
        }
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  // Deleting Crimes: Create the context menu <18.2>
  @Override
  public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
    getActivity().getMenuInflater().inflate(R.menu.crime_list_item_context, menu);
  }

  // Deleting Crimes: Listening for context menu item selection <18.5>
  @Override
  public boolean onContextItemSelected(MenuItem item){
    AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
    int position = info.position;
    CrimeAdapter adapter = (CrimeAdapter)getListAdapter();
    Crime crime = adapter.getItem(position);

    switch (item.getItemId()) {
      case R.id.menu_item_delete_crime:
        CrimeLabSingleton.get(getActivity()).deleteCrime(crime);
        adapter.notifyDataSetChanged();
        return true;
    }
    return super.onContextItemSelected(item);
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setRetainInstance(true);                  // Initiate variable and retian CrimeListFragment for Rotation.
    this.subtitleVisible = false;
    setHasOptionsMenu(true);                  // enable  Options Menu <16.4>
    getActivity().setTitle(R.string.crime_title_label);

    this.crimes = CrimeLabSingleton.get(getActivity()).getCrimes();

    // ---- setting display of the ListView ---- //
    // using ArrayAdapter Layout
    //ArrayAdapter<Crime> adapter = new ArrayAdapter<Crime>(getActivity(), android.R.layout.simple_list_item_1, this.crimes);
    // using ListItem Layout
    CrimeAdapter adapter = new CrimeAdapter(this.crimes);
    setListAdapter(adapter);
  }

  // Set subtitle if subTitleVisible is true <16.19>
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
    View v = super.onCreateView(inflater, parent, savedInstanceState);
    if (this.subtitleVisible) {
      getActivity().getActionBar().setSubtitle(R.string.subtitle);
    }

    // Registering ListView for a context menu <18.3>
    ListView listView = (ListView)v.findViewById(android.R.id.list);
    //registerForContextMenu(listView);                             // used for API < HoneyComb <18.6>
    listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);    // used for API > HoneyComb
    listView.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {    // Setting the MultiChoiceModeListener <18.7>
      @Override
      public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {

      }

      // ActionMode.Callback methods
      @Override
      public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        MenuInflater inflater = mode.getMenuInflater();
        inflater.inflate(R.menu.crime_list_item_context, menu);
        return true;
      }

      @Override
      public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
      }

      // delete
      @Override
      public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        switch (item.getItemId()) {
          case R.id.menu_item_delete_crime:
            CrimeAdapter adapter = (CrimeAdapter)getListAdapter();
            CrimeLabSingleton crimeLab = CrimeLabSingleton.get(getActivity());
            for (int i = adapter.getCount() - 1; i >= 0; i--) {
              if (getListView().isItemChecked(i)) {
                crimeLab.deleteCrime(adapter.getItem(i));
              }
            }
            mode.finish();
            adapter.notifyDataSetChanged();
            return true;
          default:
            return false;
        }
      }

      @Override
      public void onDestroyActionMode(ActionMode mode) {

      }
    });
    return v;
  }

  // Adapter Class
  private class CrimeAdapter extends ArrayAdapter<Crime> {

    public CrimeAdapter(ArrayList<Crime> crimes) {
      super(getActivity(), 0, crimes);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
      // If no View provided, inflate one
      if (convertView == null) {
        convertView = getActivity().getLayoutInflater().inflate(R.layout.list_item_crime, null);
      }

      // Configure the view for this Crime
      Crime c = getItem(position);

      TextView titleTextView = (TextView)convertView.findViewById(R.id.crime_list_item_title_TextView);
      titleTextView.setText(c.getTitle());

      TextView dateTextView = (TextView)convertView.findViewById(R.id.crime_list_item_date_TextView);
      dateTextView.setText(c.getDate().toString());

      CheckBox solvedCheckBox = (CheckBox)convertView.findViewById(R.id.crime_list_item_solved_CheckBox);
      solvedCheckBox.setChecked(c.isSolved());

      return convertView;
    }
  }
}
