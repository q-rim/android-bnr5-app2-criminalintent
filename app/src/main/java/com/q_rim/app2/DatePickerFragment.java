package com.q_rim.app2;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.DatePicker;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DatePickerFragment extends DialogFragment {

  public static final String EXTRA_DATE = "com.q_rim.android.app2.date";

  private Date date;

  public static DatePickerFragment newInstance(Date date) {
    Bundle args = new Bundle();
    args.putSerializable(EXTRA_DATE, date);

    DatePickerFragment fragment = new DatePickerFragment();
    fragment.setArguments(args);

    return fragment;
  }

  // Sending data to the target Fragemnt <12.9>
  private void sendResult(int resultCode) {
    if (getTargetFragment() == null) {
      return;
    }
    Intent i = new Intent();
    i.putExtra(EXTRA_DATE, this.date);
    getTargetFragment().onActivityResult(getTargetRequestCode(), resultCode, i);
  }

  // Creating a DatePicker DialogFragment <12.2>
  @Override
  public Dialog onCreateDialog(Bundle savedInstanceState) {
    this.date = (Date)getArguments().getSerializable(EXTRA_DATE);

    // Date extraction and DatePicker initialization <12.7>
    // Create a Calendar to get the YYYY:MM:DD
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(this.date);
    int year = calendar.get(Calendar.YEAR);
    int month= calendar.get(Calendar.MONTH);
    int day = calendar.get(Calendar.DAY_OF_MONTH);

    View v = getActivity().getLayoutInflater().inflate(R.layout.dialog_date, null);

    DatePicker datePicker = (DatePicker)v.findViewById(R.id.dialog_datePicker);
    datePicker.init(year, month, day, new DatePicker.OnDateChangedListener() {
      @Override
      public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        // Translate Year, Month, Day into a Date object using a calendar
        date = new GregorianCalendar(year, monthOfYear, dayOfMonth).getTime();

        // Update arguments to preserve selected value on rotation
        getArguments().putSerializable(EXTRA_DATE, date);
      }
    });

    return new AlertDialog.Builder(getActivity())
      .setView(v)
      .setTitle(R.string.date_picker_title)
      //.setPositiveButton(android.R.string.ok, null)
      .setPositiveButton(             // Sending data to the target Fragemnt <12.9>
        android.R.string.ok, new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int which) {
            sendResult(Activity.RESULT_OK);
          }
        }
      )
      .create();
  }
}
