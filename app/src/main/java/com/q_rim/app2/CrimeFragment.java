package com.q_rim.app2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NavUtils;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import java.util.Date;
import java.util.UUID;

public class CrimeFragment extends Fragment {

  public static final String EXTRA_CRIME_ID = "com.q_rim.android.app2.crime_id";
  private static final String DIALOG_DATE = "date";
  private static final int REQUEST_DATE = 0;

  private Crime crime;
  private EditText titleField;
  private Button dateButton;
  private CheckBox solvedCheckBox;

  // Responding to the dialog
  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (resultCode != Activity.RESULT_OK) return;
    if (requestCode == REQUEST_DATE) {
      Date date = (Date)data.getSerializableExtra(DatePickerFragment.EXTRA_DATE);
      this.crime.setDate(date);
      this.dateButton.setText(this.crime.getDate().toString());
      //updateDate();
    }
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
//    crime = new Crime();
//    UUID crimeId = (UUID)getActivity().getIntent().getSerializableExtra(EXTRA_CRIME_ID);
    UUID crimeId = (UUID)getArguments().getSerializable(EXTRA_CRIME_ID);    // Get crime ID from the arguments.
    crime = CrimeLabSingleton.get(getActivity()).getCrime(crimeId);
    setHasOptionsMenu(true);
  }

  // Responding to the UP/Back ActionBar button <16.12>
  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        if (NavUtils.getParentActivityName(getActivity()) != null) {    // if there's no parent named in metadata, don't display <--
          NavUtils.navigateUpFromSameTask(getActivity());
        }
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.fragment_crime, parent, false);

    // Turn on UP(BACK) Button <16.8>
    getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);

    this.titleField = (EditText)v.findViewById(R.id.crimeTitle_EditText);
    this.titleField.setText(this.crime.getTitle());
    this.titleField.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
        crime.setTitle(s.toString());
      }

      @Override
      public void afterTextChanged(Editable s) {

      }
    });

    // DialogFragment displayed.  <12.3>
    this.dateButton = (Button)v.findViewById(R.id.crimeDate_Button);
    this.dateButton.setText(this.crime.getDate());
    // updateDate();
    //dateButton.setEnabled(false);
    this.dateButton.setOnClickListener(new View.OnClickListener() {
      public void onClick(View v) {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        //DatePickerFragment dialog = new DatePickerFragment();
        DatePickerFragment dialog = DatePickerFragment.newInstance(crime.getDateDate());
        dialog.setTargetFragment(CrimeFragment.this, REQUEST_DATE);         // set target Fragment <12.8>
        dialog.show(fm, DIALOG_DATE);
      }
    });

    this.solvedCheckBox = (CheckBox)v.findViewById(R.id.crimeSolved_CheckBox);
    this.solvedCheckBox.setChecked(this.crime.isSolved());
    this.solvedCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        crime.setSolved(isChecked);
        Log.d("----------", "CrimeFragment.java:  crime.isSolved() = "+crime.isSolved());
      }
    });
    return v;
  }

  // ---- Attaching args to fragment ---- //
  public static CrimeFragment newInstance(UUID crimeId) {
    Bundle args = new Bundle();
    args.putSerializable(EXTRA_CRIME_ID, crimeId);

    CrimeFragment fragment = new CrimeFragment();
    fragment.setArguments(args);

    return fragment;
  }

  // Saving Crimes to filename before it is destroyed by onStop() or onDestroy()
  @Override
  public void onPause() {
    super.onPause();
    CrimeLabSingleton.get(getActivity()).saveCrimes();
  }
}