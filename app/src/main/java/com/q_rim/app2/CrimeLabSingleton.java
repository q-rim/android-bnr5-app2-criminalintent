package com.q_rim.app2;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.UUID;

public class CrimeLabSingleton {
  private static final  String TAG = "--------- CrimeLab";
  private static final String FILENAME = "crimes.json";

  private ArrayList<Crime> crimes;
  private CriminalIntentJSONSerializer serializer;

  private static CrimeLabSingleton crimeLab;
  private Context appContext;

  private CrimeLabSingleton(Context appContext) {
    this.appContext = appContext;
    this.serializer = new CriminalIntentJSONSerializer(this.appContext, FILENAME);

    try {
      this.crimes = this.serializer.loadCrimes();
    } catch (Exception e) {
      this.crimes = new ArrayList<Crime>();
      Log.e(TAG, "Error loading crimes: ", e);
    }
  }

  public static CrimeLabSingleton get(Context c) {
    if (crimeLab == null) {
      crimeLab = new CrimeLabSingleton(c.getApplicationContext());
    }
    return crimeLab;
  }

  // Adding a new crime <16.5>
  public void addCrime(Crime c) {
    crimes.add(c);
  }

  // Deleting a crime <18.4>
  public void deleteCrime(Crime c) {
    this.crimes.remove(c);
  }

  public ArrayList<Crime> getCrimes() {
    return this.crimes;
  }

  public Crime getCrime(UUID id) {
    for (Crime c : this.crimes) {
      if (c.getId().equals(id)) {
        return c;
      }
    }
    return null;
  }

  // Saving changes persistantly in CrimeLab <17.3>
  public boolean saveCrimes() {
    try{
      this.serializer.saveCrimes(this.crimes);
      Log.d(TAG, "crimes saved to file");
      return true;
    } catch (Exception e) {
      Log.e(TAG, "Error saving crimes: ", e);
//      Toast.makeText(SingleFragmentActivity.this.getActivity(), "¯\\_(ツ)_/¯", Toast.LENGTH_SHORT).show();
      return false;
    }
  }
}