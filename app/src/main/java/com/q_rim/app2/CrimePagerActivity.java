package com.q_rim.app2;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;

import java.util.ArrayList;
import java.util.UUID;

public class CrimePagerActivity extends FragmentActivity {
  private ViewPager viewPager;
  private ArrayList<Crime> crimes;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    // Create View using code instead of layout.xml <11.3>
    this.viewPager = new ViewPager(this);
    this.viewPager.setId(R.id.viewPager);
    setContentView(this.viewPager);

    this.crimes = CrimeLabSingleton.get(this).getCrimes();

    // Set up page adapter <11.4>
    FragmentManager fm = getSupportFragmentManager();
    this.viewPager.setAdapter(new FragmentStatePagerAdapter(fm) {
      @Override
      public int getCount() {
        return crimes.size();
      }

      @Override
      public Fragment getItem(int position) {
        Crime crime = crimes.get(position);
        return CrimeFragment.newInstance(crime.getId());
      }
    });

    // Selecting the item on ListView will goto the DetailedView with correct Crime#.  <11.7>
    UUID crimeId = (UUID)getIntent().getSerializableExtra(CrimeFragment.EXTRA_CRIME_ID);
    for (int i = 0; i < this.crimes.size(); i++) {
      if (this.crimes.get(i).getId().equals(crimeId)) {
        this.viewPager.setCurrentItem(i);
        break;
      }
    }
  }
}