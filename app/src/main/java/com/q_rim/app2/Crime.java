package com.q_rim.app2;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.UUID;

public class Crime {

  private static final String JSON_ID = "id";
  private static final String JSON_TITLE = "title";
  private static final String JSON_SOLVED = "solved";
  private static final String JSON_DATE = "date";

  public static final String TAG = "---Crime.java";

  private UUID id;
  private String title;
  private Date date;
  private boolean solved;

  public Crime() {
    // Generate unique id
    this.id = UUID.randomUUID();
    this.date = new Date();
  }

  public Crime(JSONObject json) throws JSONException {
    this.id = UUID.fromString(json.getString(JSON_ID));
    if (json.has(JSON_TITLE)) {
      this.title = json.getString(JSON_TITLE);
    }
    this.solved = json.getBoolean(JSON_SOLVED);
    this.date = new Date(json.getLong(JSON_DATE));
  }

  // JSON serialization
  public JSONObject toJSON() throws JSONException {
    JSONObject json = new JSONObject();
    json.put(JSON_ID, this.id.toString());
    json.put(JSON_TITLE, this.title);
    json.put(JSON_SOLVED, this.solved);
    json.put(JSON_DATE, this.date.getTime());
    return json;
  }

  public UUID getId() {
    return this.id;
  }

  public String getTitle() {
    Log.d(TAG, "getTitle="+this.title);
    return this.title;
  }

  public void setTitle(String title) {
    Log.d(TAG, "setTitle="+title);
    this.title = title;
  }

  public String getDate() {
    String[] dateSplit = date.toString().split("\\s+");     // split date components.
    String[] timeSplit = dateSplit[3].split(":");           // change HH:MM:SS  into  HH:MM
    // Stitch together the values
    String d = dateSplit[0] +", "+ dateSplit[1] +" "+ dateSplit[2] + ", " + dateSplit[5] + "    " + timeSplit[0] + ":" + timeSplit[1];
    return d;
  }

  public Date getDateDate() {
    return this.date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public boolean isSolved() {
    return this.solved;
  }

  public void setSolved(boolean solved) {
    this.solved = solved;
  }

  @Override
  public String toString() {
    return this.title;
  }
}
