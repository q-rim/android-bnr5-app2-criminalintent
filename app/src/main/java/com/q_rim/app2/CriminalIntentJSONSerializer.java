package com.q_rim.app2;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;

public class CriminalIntentJSONSerializer {
  private Context context;
  private String fileName;

  public CriminalIntentJSONSerializer(Context c, String f) {
    this.context = c;
    this.fileName = f;
  }

  // Saving
  public void saveCrimes(ArrayList<Crime> crimes) throws JSONException, IOException {

    // Bulid an array in JSON
    JSONArray array = new JSONArray();
    int sum = 0;
    for (Crime c : crimes)
      array.put(c.toJSON());    // Create JSON from Array

    // Write the file to disk
    Writer writer = null;
    try{
      OutputStream out = this.context.openFileOutput(this.fileName, Context.MODE_PRIVATE);    // Open File
      writer = new OutputStreamWriter(out);
      writer.write(array.toString());           // Write to file
    } finally {
      if (writer != null) {
        writer.close();           // Close file
      }
    }
  }

  // Loading
  public ArrayList<Crime> loadCrimes() throws IOException, JSONException {
    ArrayList<Crime> crimes = new ArrayList<Crime>();
    BufferedReader reader = null;
    try{
      // Open and read the file into a String Reader
      InputStream in = this.context.openFileInput(this.fileName);
      reader = new BufferedReader(new InputStreamReader(in));
      StringBuilder jsonString = new StringBuilder();
      String line = null;
      while ((line = reader.readLine()) != null) {
        // Line breaks are omitted and irrelevant
        jsonString.append(line);
      }

      // Parse JSON using JSONTokener
      JSONArray array = (JSONArray) new JSONTokener(jsonString.toString()).nextValue();
      // Build array of crimes from JSONObjects
      for (int i = 0; i < array.length(); i++) {
        crimes.add(new Crime(array.getJSONObject(i)));
      }
    } catch (FileNotFoundException e) {
      // Ignore this one;  it happens when starting fresh
    } finally {
      if (reader != null)
        reader.close();
    }
    return crimes;
  }
}
